%include 'io.inc'
%include 'gfx.inc'
%include 'util.inc'

%define WIDTH  1024
%define threeWIDTH 3072
%define HEIGHT 768

%define menuLeft 197
%define menuOffset 40

global main

section .text

; reads a bmp file
; input parameters: esi - pointer to the file name
; output parameters: imgWidth, imgHeight, imgBytes, img
readImg:
	pusha
	mov eax, esi
	
	xor ebx, ebx ; read mode
	call fio_open
	
	; read the 54 byte bmp header to get the dimensions of the img
	mov ebx, bmpHeader
	xor ecx, ecx
	mov ecx, 54
	call fio_read
	
	push eax ; file handle
	xor eax, eax
	mov eax, bmpHeader
	
	; the 19th byte shows width
	mov ecx, 18
	.getWidth:
		inc eax
		loop .getWidth
	xor esi, esi
	mov esi, [eax]
	mov dword[imgWidth], esi
	
	; the 23th byte shows height
	mov ecx, 4
	.getHeight:
		inc eax
		loop .getHeight
	xor edi, edi
	mov edi, [eax]
	mov dword[imgHeight], edi
	
	pop eax ; file handle	
	
	; read the bitmap of the img
	
	; calculate number of bytes to read
	xor ecx, ecx
	imul ecx, esi, 3
	imul ecx, edi
	
	mov dword[imgBytes], ecx

	mov ebx, img
	call fio_read

	call fio_close
	
	popa
	ret

; draws a bitmap
; input parameters: ecx - number of bytes to draw | img - pointer to the start of the bitmap
drawImg:
	pusha 
	
	dec ecx
	; point to the end of the bitmap
	lea esi, [img+ecx]
	
	; Main loop
	.mainloop:
		; Draw something
		call	gfx_map			; map the framebuffer -> EAX will contain the pointer
		
		; Loop over the lines
		xor		ecx, ecx		; ECX - line (Y)
	.yloop:
		cmp		ecx, HEIGHT
		jge		.yend	
	
		; get one row of the img
		xor edi, edi
		lea edi, [esi-threeWIDTH+1]
	
		; Loop over the columns
		xor		edx, edx		; EDX - column (X)
	.xloop:
		cmp		edx, WIDTH
		jge		.xend
		
		; Write the pixel
		; blue
		mov 	ebx, [edi]
		mov		[eax], bl
		; green
		mov 	ebx, [edi+1]
		mov		[eax+1], bl
		; red
		mov 	ebx, [edi+2]
		mov		[eax+2], bl
		; zero
		xor		ebx, ebx
		mov		[eax+3], bl
		
		; next pixel
		add		eax, 4
		add 	edi, 3
		
		inc		edx
		jmp		.xloop
		
	.xend:
		inc		ecx
		sub esi, threeWIDTH
		
		jmp		.yloop
		
	.yend:
		call	gfx_unmap		; unmap the framebuffer
		call	gfx_draw		; draw the contents of the framebuffer (*must* be called once in each iteration!)
		
	popa
	ret

main:
	xor esi, esi
	mov esi, startScreen
	call readImg
	
	; Create the graphics window
    mov		eax, WIDTH		; window width (X)
	mov		ebx, HEIGHT		; window height (Y)
	mov		ecx, 0			; window mode (NOT fullscreen!)
	mov		edx, caption	; window caption
	call	gfx_init
	
	test	eax, eax		; if the return value is 0, something went wrong
	jnz		.init
	; Print error message and exit
	mov		eax, errormsg
	call	io_writestr
	call	io_writeln
	ret
		
	.init:
	mov ecx, dword[imgBytes]
	call drawImg
	
	.eventloop:
		call	gfx_getevent
		push eax
		
		; hover text in menu
		
		; get mouse current position
		call gfx_getmouse
		xor edx, edx
		mov edx, eax
		pop eax
		
		cmp ebx, 592
		jb .noHover
		cmp ebx, 658
		ja .noHover
		cmp edx, menuLeft
		jb .noHover
		
		mov ecx, menuLeft
		; play hover
		add ecx, 111
		cmp edx, ecx
		ja .score_hover
		
		mov esi, focus_play
		call readImg
		jmp .next
		
		.score_hover:
		add ecx, menuOffset
		cmp edx, ecx
		jbe .noHover
		
		; scoreboard hover
		add ecx, 217
		cmp edx, ecx
		ja .help_hover
		
		mov esi, focus_score
		call readImg
		jmp .next
		
		.help_hover:
		add ecx, menuOffset
		cmp edx, ecx
		jbe .noHover
		
		; help hover
		add ecx, 100
		cmp edx, ecx
		ja .quit_hover
		
		mov esi, focus_help
		call readImg
		jmp .next		
		
		.quit_hover:
		add ecx, menuOffset
		cmp edx, ecx
		jbe .noHover
		
		; quit hover
		add ecx, 96
		cmp edx, ecx
		ja .noHover
		
		mov esi, focus_quit
		call readImg
		
		; left button pressed
		cmp		eax, 1
		je .end
		
		jmp .next		
	
		.noHover:
		mov esi, startScreen
		call readImg
		
		.next:
		
		; Handle exit
		cmp		eax, 23			; the window close button was pressed: exit
		je		.end
		cmp		eax, 27			; ESC: exit
		je		.end
		test	eax, eax		; 0: no more events
		jnz		.eventloop
	
	jmp .init
	
	.end:
	call gfx_destroy
	
	ret
	
section .data
    caption db "The Cookie Monster pacman", 0
	errormsg db "ERROR: could not initialize graphics!", 0
	; menu
	startScreen db ".\img\start.bmp", 0
	focus_play db ".\img\play.bmp", 0
	focus_score db ".\img\scoreboard.bmp", 0
	focus_help db ".\img\help.bmp", 0
	focus_quit db ".\img\quit.bmp", 0
	; read img
	imgBytes dd 0
	imgWidth dd 0
	imgHeight dd 0
section .bss
	bmpHeader resb 54
	oneRow resb WIDTH
	img resq 294912